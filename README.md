Collapses the sidebars in Discord if the cursor isn't hovering over them.

Can be install with the Stylus extention for [Firefox][stylus-ff] or [Chrome][stylus-cr]. First, install the relevant extention, then navigate to: https://gitlab.com/GrantMoyer/collapsing-sidebars-for-discord/-/raw/master/collapsing_sidebars.user.css and select "Install Style". 

[stylus-ff]: https://addons.mozilla.org/en-US/firefox/addon/styl-us/
[stylus-cr]: https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne